<?php

/**
 * @file
 * Provides formatters to output converted values from number fields.
 */

/**
 * Implements hook_field_formatter_info().
 *
 * @see number_field_formatter_info()
 */
function feet_inches_formatter_field_formatter_info() {
  return array(
    'feet_inches_formatter_default' => array(
      'label'       => t('Inches converted to feet + inches'),
      'field types' => array(
        'number_integer',
        'number_decimal',
        'number_float',
      ),
      'settings'    => array(
        'suffix_feet'   => "'",
        'suffix_inches' => '"',
        'hide_zero'     => TRUE,
        'joiner'        => ' x ',
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function feet_inches_formatter_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $summary = array();

  switch ($display['type']) {
    case 'feet_inches_formatter_default':
      $values = array();
      if ($settings['hide_zero']) {
        $values[] = 5 . $settings['suffix_feet'];
      }
      else {
        $values[] = 5 . $settings['suffix_feet'] . 0 . $settings['suffix_inches'];
      }
      $values[] = 5 . $settings['suffix_feet'] . 8 . $settings['suffix_inches'];
      $summary[] = implode($settings['joiner'], $values);
      break;
  }

  $context = array(
    'field'     => $field,
    'instance'  => $instance,
    'view_mode' => $view_mode,
  );
  drupal_alter('feet_inches_formatter_field_formatter_settings_form', $summary, $context);

  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_settings_form().
 *
 * @see number_field_formatter_settings_form()
 */
function feet_inches_formatter_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  if ($display['type'] == 'feet_inches_formatter_default') {

    $element['suffix_feet'] = array(
      '#type'          => 'textfield',
      '#size'          => 10,
      '#title'         => t('Feet suffix'),
      '#default_value' => $settings['suffix_feet'],
    );
    $element['suffix_inches'] = array(
      '#type'          => 'textfield',
      '#size'          => 10,
      '#title'         => t('Inches suffix'),
      '#default_value' => $settings['suffix_inches'],
    );
    $element['joiner'] = array(
      '#type'          => 'textfield',
      '#size'          => 10,
      '#title'         => t('Join multiple values with'),
      '#default_value' => $settings['joiner'],
    );
    $element['hide_zero'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Hide zero inches?'),
      '#default_value' => $settings['hide_zero'],
    );

    return $element;
  }
}

/**
 * Implements hook_field_formatter_view().
 *
 * @see number_field_formatter_view()
 */
function feet_inches_formatter_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'];

  if ($display['type'] === 'feet_inches_formatter_default') {
    foreach ($items as $delta => $item) {
      $feet = floor($item['value'] / 12);
      $inches = $item['value'] - ($feet * 12);
      $args = array(
        '@feet'          => $feet,
        '@feet_suffix'   => $settings['suffix_feet'],
        '@inches'        => $inches,
        '@inches_suffix' => $settings['suffix_inches'],
      );
      if ($settings['hide_zero'] && $inches == 0) {
        $output[] = t('@feet@feet_suffix', $args);
      }
      else {
        $output[] = t('@feet@feet_suffix@inches@inches_suffix', $args);
      }
    }
    $element[0] = array(
      '#prefix' => $instance['settings']['prefix'],
      '#suffix' => $instance['settings']['suffix'],
      '#markup' => implode($settings['joiner'], $output),
    );
  }

  return $element;
}
