# Drupal Module: Feet Inches Formatter
**Author:** Aaron Klump  <sourcecode@intheloftstudios.com>

## Summary
**A display formatter for number fields to output as feet + inches.**

You may also visit the [project page](http://www.drupal.org/project/feet_inches_formatter) on Drupal.org.

## Requirements

## Installation
1. Install as usual, see [http://drupal.org/node/70151](http://drupal.org/node/70151) for further information.

## Configuration
1. Set this as the display formatter for a number field.
1. You can choose what to use for the feet and inches suffix.
1. You can choose what to use for combining multiple values, defaults to ' x '.

## Design Decisions/Rationale
1. Wanted to store as inches.
1. Wanted to display as feet + inches.
1. Couldn't find a module already doing this.

## Contact
* **In the Loft Studios**
* Aaron Klump - Developer
* PO Box 29294 Bellingham, WA 98228-1294
* _skype_: intheloftstudios
* _d.o_: aklump
* <http://www.InTheLoftStudios.com>
